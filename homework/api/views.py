from django.shortcuts import render
from rest_framework import generics
from blogs.models import Post
from django.contrib.auth.models import User
from .serializers import PostSerializer
from rest_framework import permissions

#List all Posts or create new Post
class PostList(generics.ListCreateAPIView):
   	queryset = Post.objects.all()
	serializer_class = PostSerializer
	permission_classes = (permissions.IsAuthenticated,)
	paginate_by = 10

#Retrieve, update or delete a post instance
class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated,)
    
#Retrieve Posts by selected user.
class UserPostList(generics.ListAPIView):
	queryset = Post.objects.all()
	serializer_class = PostSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		queryset = super(UserPostList, self).get_queryset()
		return queryset.filter(author__username=self.kwargs.get('username'))



