"""
WSGI config for homework project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ["DJANGO_SETTINGS_MODULE"] = "homework.live_settings"
os.environ["DB_NAME"] = os.path.join(BASE_DIR, 'db.sqlite3')
os.environ["DJANGO_SECRET_KEY"] = "n7%-ws*cv18)#suf&swp22ak=$((yz-ou$1$$yf)n-_gk1!4+)"

application = get_wsgi_application()
